/* eslint-disable prettier/prettier */
import { lazy } from 'react';

// project imports
import MainLayout from 'layout/MainLayout';
import Loadable from 'ui-component/Loadable';

// dashboard routing
const DashboardDefault = Loadable(lazy(() => import('views/dashboard/Default')));
const PatientForm = Loadable(lazy(() => import('views/dashboard/Default/patient/PatientAddForm')));
const DiabeticForm = Loadable(lazy(() => import('views/dashboard/Default/diebetic/DiabeticAddForm')));





// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
    path: '/',
    element: (
        <MainLayout />
    ),
    children: [
        {
            path: '/',
            element: <DashboardDefault />
        },
        {
            path: '/patient/add',
            element: <PatientForm />
        },
        {
            path: '/screen/add',
            element: <DiabeticForm />
        }

    ]
};

export default MainRoutes;
