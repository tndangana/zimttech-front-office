# Introduction

This is material design template created based on materially structure

# Getting Started

1. Installation process
    - install node version 16 to 18
    - run 'npm install / yarn' to download dependencies
    - run 'npm start'
    - start dev server run 'npm run start / yarn start'
2. Deployment process
    - Goto full-version directory and open package.json. Update homepage URL to the production URL
    - Goto full-version directory and run 'npm run build / yarn build'
