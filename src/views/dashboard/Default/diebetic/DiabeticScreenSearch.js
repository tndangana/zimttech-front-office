/* eslint-disable prettier/prettier */
// material-ui
import {
    Grid,
    TextField,
    Typography, Box, Stack, Button} from '@mui/material';
import { Formik } from 'formik';
import * as Yup from 'yup';
import AnimateButton from 'ui-component/extended/AnimateButton';
import TagFacesIcon from '@mui/icons-material/TagFaces';
// project imports
import MainCard from 'ui-component/cards/MainCard';
import InputLabel from 'ui-component/extended/Form/InputLabel';
import { gridSpacing } from 'store/constant';
import { useDiebetic } from '../../../../hooks/api/diabeticscreening';

// ==============================|| Columns Layouts ||============================== //
function ScreeningFilter() {
    const { getFilteredList } = useDiebetic();
    const handleSubmit = async (param) => {

        const payload = {
            bloodGlucoseMax: param.bloodGlucoseMax,
            bloodGlucoseMin: param.bloodGlucoseMin,
            bloodPressureMax: param.bloodPressureMax,
            bloodPressureMin: param.bloodPressureMin,
            heightMax: param.heightMax,
            heightMin: param.heightMin,
            weightMax: param.weightMax,
            weightMin: param.weightMin
        };
        await getFilteredList(payload);
    }

    return (
        <>
            <Formik
                initialValues={{
                    bloodGlucoseMax: "",
                    bloodGlucoseMin: "",
                    bloodPressureMax: "",
                    bloodPressureMin: "",
                    heightMax: "",
                    heightMin: "",
                    weightMax: "",
                    weightMin: ""
                }}

                validationSchema={Yup.object().shape({
                    bloodGlucoseMax: Yup.number().when('bloodGlucoseMin', {
                        is: (val) => val != null && val !== '',
                        then: Yup.number().moreThan(Yup.ref('bloodGlucoseMin'), 'Max value must be greater than min value'),
                        otherwise: Yup.number(),
                    }),
                    bloodGlucoseMin: Yup.number(),
                    bloodPressureMax: Yup.number().when('bloodPressureMin', {
                        is: (val) => val != null && val !== '',
                        then: Yup.number().moreThan(Yup.ref('bloodPressureMin'), 'Max value must be greater than min value'),
                        otherwise: Yup.number(),
                    }),
                    bloodPressureMin: Yup.number(),
                    heightMax: Yup.number().when('heightMin', {
                        is: (val) => val != null && val !== '',
                        then: Yup.number().moreThan(Yup.ref('heightMin'), 'Max value must be greater than min value'),
                        otherwise: Yup.number(),
                    }),
                    heightMin: Yup.number(),
                    weightMax: Yup.number().when('weightMin', {
                        is: (val) => val != null && val !== '',
                        then: Yup.number().moreThan(Yup.ref('weightMin'), 'Max value must be greater than min value'),
                        otherwise: Yup.number(),
                    }),
                    weightMin: Yup.number(),
                })}

                onSubmit={values => {
                    try {
                        handleSubmit(values);
                    } catch (error) {
                        console.error(error)
                    }
                }}
            >
                {({ errors, handleChange, handleSubmit, values, touched }) => (
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12}>
                            <MainCard title=" Diebetic Screening Filter">
                                <Box sx={{ width: '100%', maxWidth: 500 }}>
                                    <Typography variant="h5" style={{ color: 'red' }} gutterBottom>
                                        Select the threshold (s) you only want.The query  only searches for patients who are 18+ and have been on treatment for the past 18 months.If you click the query button without putting any threshold all patients with treatment over 18 months will be displayed. Enjoy  <TagFacesIcon/>
                                    </Typography>
                                </Box>

                                <form noValidate onSubmit={handleSubmit} >
                                    <Grid container spacing={2} alignItems="center">
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Weight Maximum</InputLabel>
                                            <TextField fullWidth placeholder="Enter your weight value"
                                                name='weightMax'
                                                value={values.weightMax}
                                                onChange={handleChange}
                                                error={touched.weightMax && Boolean(errors.weightMax)}
                                                helperText={touched.weightMax && errors.weightMax} />
                                        </Grid>
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Weight Minimum</InputLabel>
                                            <TextField fullWidth placeholder="Enter your weight value"
                                                name='weightMin'
                                                value={values.weightMin}
                                                onChange={handleChange}
                                                error={touched.weightMin && Boolean(errors.weightMin)}
                                                helperText={touched.weightMin && errors.weightMin} />
                                        </Grid>
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Height Maximum</InputLabel>
                                            <TextField fullWidth placeholder="Enter height value"
                                                name='heightMax'
                                                value={values.heightMax}
                                                onChange={handleChange}
                                                error={touched.heightMax && Boolean(errors.heightMax)}
                                                helperText={touched.heightMax && errors.heightMax} />
                                        </Grid>
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Height Minimum</InputLabel>
                                            <TextField fullWidth placeholder="Enter  height value"
                                                name='heightMin'
                                                value={values.heightMin}
                                                onChange={handleChange}
                                                error={touched.heightMin && Boolean(errors.heightMin)}
                                                helperText={touched.heightMin && errors.heightMin} />
                                        </Grid>
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Blood Pressure Maximum</InputLabel>
                                            <TextField fullWidth placeholder="Please enter your blood pressure value"
                                                name='bloodPressureMax'
                                                value={values.bloodPressureMax}
                                                onChange={handleChange}
                                                error={touched.bloodPressureMax && Boolean(errors.bloodPressureMax)}
                                                helperText={touched.bloodPressureMax && errors.bloodPressureMax} />
                                        </Grid>
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Blood Pressure Minimum</InputLabel>
                                            <TextField fullWidth placeholder="Enter your blood pressure value"
                                                name='bloodPressureMin'
                                                value={values.bloodPressureMin}
                                                onChange={handleChange}
                                                error={touched.bloodPressureMin && Boolean(errors.bloodPressureMin)}
                                                helperText={touched.bloodPressureMin && errors.bloodPressureMin} />
                                        </Grid>
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Blood Glucose Maximum</InputLabel>
                                            <TextField fullWidth placeholder="Enter your blood glucose value"
                                                name='bloodGlucoseMax'
                                                value={values.bloodGlucoseMax}
                                                onChange={handleChange}
                                                error={touched.bloodGlucoseMax && Boolean(errors.bloodGlucoseMax)}
                                                helperText={touched.bloodGlucoseMax && errors.bloodGlucoseMax} />
                                        </Grid>
                                        <Grid item xs={12} lg={4}>
                                            <InputLabel>Blood Glucose Minimum</InputLabel>
                                            <TextField fullWidth placeholder="Enter your blood glucose value"
                                                name='bloodGlucoseMin'
                                                value={values.bloodGlucoseMin}
                                                onChange={handleChange}
                                                error={touched.bloodGlucoseMin && Boolean(errors.bloodGlucoseMin)}
                                                helperText={touched.bloodGlucoseMin && errors.bloodGlucoseMin} />
                                        </Grid>
                                    </Grid>
                                    <Stack
                                        direction={{ xs: 'column', sm: 'row' }}
                                        spacing={{ xs: 1, sm: 2, md: 4 }} >
                                        <Box sx={{ mt: 2 }}>
                                            <AnimateButton>
                                                <Button
                                                    disableElevation
                                                    fullWidth
                                                    size="large"
                                                    type="submit"
                                                    variant="contained"
                                                    color="primary"
                                                >
                                                    Query
                                                </Button>
                                            </AnimateButton>
                                        </Box>
                                    </Stack>
                                </form>
                            </MainCard>
                        </Grid>
                    </Grid>)}
            </Formik >
        </>
    );
}

export default ScreeningFilter;
