/* eslint-disable no-unused-expressions */
/* eslint-disable prettier/prettier */
import axios from 'axios';

export const axiosMain = axios.create({
  baseURL: 'http://localhost:9090',
  headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization'
  }
});

const axiosServices = axios.create({ baseURL: process.env.REACT_APP_API_URL || 'http://localhost:3010/' });


// axiosMain.interceptors.response.use(
//   (response) => response,
//   (error) => Promise.reject((error.response && error.response.data) || 'Wrong Services')
// );

export default axiosServices;
